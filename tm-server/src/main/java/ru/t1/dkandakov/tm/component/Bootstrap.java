package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.service.*;
import ru.t1.dkandakov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.dkandakov.tm.endpoint.AbstractEndpoint;
import ru.t1.dkandakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectServiceDTO projectService;

    @NotNull
    @Autowired
    private ITaskServiceDTO taskService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IUserServiceDTO userService;

    @NotNull
    @Autowired
    private ISessionServiceDTO sessionService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }


    private void registry(@NotNull final Object endpoint) {
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?WSDL",
                propertyService.getServerHost(),
                propertyService.getServerPort(),
                name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    public void start() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        initEndpoints();
        loggerService.initJmsLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

}