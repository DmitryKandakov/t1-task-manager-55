package ru.t1.dkandakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

    void remove(@NotNull String userId, @NotNull M model);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);


}
