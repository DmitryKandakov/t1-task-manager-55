package ru.t1.dkandakov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.repository.model.ISessionRepository;
import ru.t1.dkandakov.tm.api.service.model.ISessionService;
import ru.t1.dkandakov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkandakov.tm.model.Session;

import javax.persistence.EntityManager;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    //public SessionService(@NotNull final IConnectionService connectionService) {
    //    super(connectionService);
    //  }

    @NotNull
    @Override
    public ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ISessionRepository.class);
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
