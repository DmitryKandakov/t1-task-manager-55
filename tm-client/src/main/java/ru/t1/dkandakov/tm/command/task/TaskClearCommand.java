package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskClearRequest;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}