package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataYamlSaveFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

@Component
public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in YAML file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE YAML DATA FASTER]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return "data-save-yaml";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}