package ru.t1.dkandakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.service.ICommandService;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    protected ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return propertyService;
    }

}