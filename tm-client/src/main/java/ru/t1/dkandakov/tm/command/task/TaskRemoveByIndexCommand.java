package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().removeTaskByIndex(new TaskRemoveByIndexRequest(getToken(), index));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

}