package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}