package ru.t1.dkandakov.tm.api.service;

import ru.t1.dkandakov.tm.api.repository.IRepository;
import ru.t1.dkandakov.tm.dto.model.AbstractModelDTO;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {
}
