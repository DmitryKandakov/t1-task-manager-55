package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, status);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project's status by id.";
    }

}