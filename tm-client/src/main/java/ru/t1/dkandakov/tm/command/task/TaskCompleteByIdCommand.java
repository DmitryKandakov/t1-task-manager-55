package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken(), id, Status.COMPLETED);
        getTaskEndpoint().completeTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

}