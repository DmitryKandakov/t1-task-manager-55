package ru.t1.dkandakov.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkandakov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;



    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

}
