package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataXmlSaveJaxBRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

@Component
public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxBRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}