package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by id.";
    }

}