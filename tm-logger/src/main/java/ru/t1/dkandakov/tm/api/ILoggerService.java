package ru.t1.dkandakov.tm.api;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void log(@Nullable String message);

}
